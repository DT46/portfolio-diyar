let companies =
`[
    {
        "name": "Lisa",
        "surname": "Waterringen",
        "age": "21",
        "city": "Amsterdam",
        "eyeColor": "Brown"
    },
    {
        "name": "Jordy",
        "surname": "Manders",
        "age": "23",
        "city": "Rotterdam",
        "eyeColor": "Green"
    },
    {
        "name": "Mia",
        "surname": "Straten",
        "age": "22",
        "city": "Arnhem",
        "eyeColor": "Blue"
    },
    {
        "name": "Levi",
        "surname": "Dam",
        "age": "29",
        "city": "Zwolle",
        "eyeColor": "Black"
    },
    {
        "name": "Sjoerd",
        "surname": "Kroon",
        "age": "26",
        "city": "Enschede",
        "eyeColor": "Blue"
    },
    {
        "name": "Nick",
        "surname": "Banders",
        "age": "25",
        "city": "Venlo",
        "eyeColor": "Black"
    },
    {
        "name": "Ozan",
        "surname": "Yilmaz",
        "age": "28",
        "city": "Groningen",
        "eyeColor": "Brown"
    },
    {
        "name": "Julie",
        "surname": "Meijers",
        "age": "27",
        "city": "Roermond",
        "eyeColor": "Green"
    },
    {
        "name": "Evelien",
        "surname": "Cox",
        "age": "23",
        "city": "Eindhoven",
        "eyeColor": "Black"
    },
    {
        "name": "Masha",
        "surname": "Schepper",
        "age": "25",
        "city": "Rotterdam",
        "eyeColor": "Brown"
    }
]`

console.log(JSON.parse(companies)[7].name)